#include "sansi.h"
#include <stdio.h>
#include <stdint.h> // For uint64_t type

int main() {
    printf("Press any key to discover its keycode. Press CTRL+C to exit.\n");

    // Ensure raw mode is enabled for accurate key detection
    sansi_mode_raw(1);

    while (1) {
        uint64_t keycode = sansi_read_key(1.0);
        printf("Keycode discovered: 0x%lx\n", keycode);
        if (keycode == 0x3) // ctrl+c
            break;
    }

    // It's good practice to disable raw mode before exiting, although not strictly necessary here since the program is intended to be exited via CTRL+C
    sansi_mode_raw(0);

    return 0;
}
