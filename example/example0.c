#include <unistd.h> // For sleep()
#include "sansi.h"
#include <stdio.h>

int main() {
    // Get terminal size
    sansi_rowcol_t term_size = sansi_terminal_size();
    printf("Terminal size: width = %d, height = %d\n", term_size.column, term_size.row);
    sansi_rowcol_t curloc = sansi_cursor_query();
    printf("Cursor location: column = %d, row = %d\n", curloc.column, curloc.row);
    sleep(2); // Pause to see the output

    // Test cursor visibility toggle
    sansi_cursor_hide();
    printf("Cursor should now be hidden. Waiting...\n");
    sleep(2);

    sansi_cursor_show();
    printf("Cursor should now be visible. Waiting...\n");
    sleep(2);

    // Test cursor movements
    sansi_cursor_down(2);
    sansi_cursor_right(10);
    printf("Moved cursor down 2 and right 50. Waiting...");
    fflush(stdout);
    sleep(2);

    printf("Will soon move cursor up. Waiting...");
    fflush(stdout);
    sleep(2);
    sansi_cursor_up(2);
    fflush(stdout);
    sleep(2);
    printf("Here is where the cursor was moved to. Waiting...");
    fflush(stdout);
    sleep(2);

    sansi_cursor_left(5);
    printf("Moved cursor up 2 and left 10. Waiting...");
    fflush(stdout);
    sleep(2);

    sansi_cursor_abs(0, 0);
    printf("Moved cursor home");
    fflush(stdout);
    sleep(2);


    // Test erasing functions
    sansi_erase_line();
    sleep(1);
    sansi_erase_screen();
    printf("Screen should now be cleared.\n");
    sleep(2);

    // Test color and style
    sansi_color_t red = {255, 0, 0};
    sansi_color_t green = {0, 255, 0};
    sansi_color_fg_rgb(red); // Red text
    sansi_color_bg_rgb(green); // Green background
    printf("This should be red text on a green background.\n");
    sansi_style_reset();

    // Test style functions
    sansi_style_bold(1);
    printf("This text should be bold.\n");
    sansi_style_reset();

    sansi_style_underline(1);
    printf("This text should be underlined.\n");
    sansi_style_reset();

    // Reset styles and colors before exiting
    sansi_style_reset();

    sleep(2);
    sansi_reset();
    printf("Terminal is reset.\n");

    return 0;
}
