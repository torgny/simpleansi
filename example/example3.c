#include "nsansi.h"
#include <stdio.h>
#include <assert.h>

int main() {
    nsansi_window_t* screen = nsansi_window_screen();
    
    nsansi_window_t* stuff = nsansi_window_new_subwindow(screen, (sansi_rowcol_t){2, 2}, (sansi_rowcol_t){20, 40});

    nsansi_region_t* region = nsansi_window_new_region(stuff, (sansi_rowcol_t){2, 2}, (sansi_rowcol_t){10, 20});
    
    nsansi_region_print(region, (sansi_rowcol_t){1, 1}, L"Hello, world!", 50, '-');
    nsansi_region_paint_all(region, (nsansi_color_t){255, 32, 32, 0}, 1);
    nsansi_region_paint_all(region, (nsansi_color_t){25, 32, 32, 0}, 0);

    nsansi_window_sync(screen);

    nsansi_region_print(region, (sansi_rowcol_t){5, 2}, L"Goodbye...", -1, 0);

    nsansi_window_sync(screen);

    nsansi_window_free(screen);
}