#include "sansi.h"
#include <stdio.h>

int main() {
    sansi_rowcol_t sz = sansi_pixels_terminal_size();
    printf("Terminal size in pixels %dx%d\n", sz.row, sz.column);
    
    // Define a simple palette
    sansi_color_t palette[] = {
        {0, 0, 0},       // Entry 0 is transparent.
        {255, 255, 255}, // White
        {192, 32, 64}    // Red
    };
    int palette_size = sizeof(palette) / sizeof(palette[0]);

    // Define image data for "HELLO" (8x40 pixels)
    // Note: This is a simplified representation. You might need to adjust the design based on your needs.
    unsigned char image_data[] = {
        0, 1, 1, 0, 1, 1, 0, 1, 1, 1, 1, 1, 0, 1, 1, 0, 0, 0, 0, 1, 1, 0, 0, 0, 0, 0, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 1, 1, 0, 1, 1, 0, 1, 1, 0, 0, 1, 0, 1, 1, 0, 0, 0, 0, 1, 1, 0, 0, 0, 0, 1, 1, 1, 1, 1, 0, 0, 0, 2, 2, 0, 2, 2, 0, 0,
        0, 1, 1, 0, 1, 1, 0, 1, 1, 0, 0, 0, 0, 1, 1, 0, 0, 0, 0, 1, 1, 0, 0, 0, 0, 1, 1, 0, 1, 1, 0, 0, 2, 2, 2, 2, 2, 2, 2, 0,
        0, 1, 1, 1, 1, 1, 0, 1, 1, 1, 1, 0, 0, 1, 1, 0, 0, 0, 0, 1, 1, 0, 0, 0, 0, 1, 1, 0, 1, 1, 0, 0, 2, 2, 2, 2, 2, 2, 2, 0,
        0, 1, 1, 1, 1, 1, 0, 1, 1, 1, 1, 0, 0, 1, 1, 0, 0, 0, 0, 1, 1, 0, 0, 0, 0, 1, 1, 0, 1, 1, 0, 0, 0, 2, 2, 2, 2, 2, 0, 0,
        0, 1, 1, 0, 1, 1, 0, 1, 1, 0, 0, 0, 0, 1, 1, 0, 0, 0, 0, 1, 1, 0, 0, 0, 0, 1, 1, 0, 1, 1, 0, 0, 0, 0, 2, 2, 2, 0, 0, 0,
        0, 1, 1, 0, 1, 1, 0, 1, 1, 0, 0, 1, 0, 1, 1, 0, 0, 1, 0, 1, 1, 0, 0, 1, 0, 1, 1, 1, 1, 1, 0, 0, 0, 0, 0, 2, 0, 0, 0, 0,
        0, 1, 1, 0, 1, 1, 0, 1, 1, 1, 1, 1, 0, 1, 1, 1, 1, 1, 0, 1, 1, 1, 1, 1, 0, 0, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0
    };
    int width = 40;
    int height = 8;

    // Display the image
    sansi_pixels_display(image_data, width, height, palette, palette_size);

    return 0;
}
