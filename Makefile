CC=gcc
CFLAGS=-I./src -Wextra -pedantic -std=c99 \
        -fPIC -O2 -g3 -Wall -Wformat=2 -Wconversion -Wimplicit-fallthrough \
        -U_FORTIFY_SOURCE -D_FORTIFY_SOURCE=3 \
        -D_GLIBCXX_ASSERTIONS \
        -fstack-clash-protection -fstack-protector-strong \
        -Wl,-z,noexecstack \
        -Wl,-z,relro -Wl,-z,now
LDFLAGS=-L$(LIB_DIR) -lsansi
SRC_DIR=src
OBJ_DIR=obj
LIB_DIR=lib
EXE_DIR=bin
DEPS=$(wildcard $(SRC_DIR)/*.h)
SRC=$(wildcard $(SRC_DIR)/*.c)
OBJ=$(SRC:$(SRC_DIR)/%.c=$(OBJ_DIR)/%.o)
TARGET_STATIC=$(LIB_DIR)/libsansi.a
TARGET_DYNAMIC=$(LIB_DIR)/libsansi.so
EXAMPLES_SRC=$(wildcard ./example/*.c)
EXAMPLES_BIN=$(patsubst ./example/%.c,$(EXE_DIR)/%,$(EXAMPLES_SRC))

# Default target
all: $(TARGET_STATIC) $(TARGET_DYNAMIC) $(EXAMPLES_BIN)

# Compile .c to .o
$(OBJ_DIR)/%.o: $(SRC_DIR)/%.c $(DEPS)
	@mkdir -p $(OBJ_DIR)
	$(CC) -c -o $@ $< $(CFLAGS)

# Static library
$(TARGET_STATIC): $(OBJ)
	@mkdir -p $(LIB_DIR)
	ar rcs $@ $^

# Dynamic library
$(TARGET_DYNAMIC): $(OBJ)
	@mkdir -p $(LIB_DIR)
	$(CC) -shared -o $@ $^ $(CFLAGS)

# Example binaries
bin/%: ./example/%.c $(TARGET_STATIC)
	@mkdir -p bin
	$(CC) -o $@ $< -L$(LIB_DIR) -lsansi $(CFLAGS) -static

.PHONY: clean examples

clean:
	rm -rf $(OBJ_DIR) $(LIB_DIR) $(EXE_DIR)
