#include "nsansi.h"
#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <assert.h>
#include <errno.h>

#define SANSI_COLOR_FG_RGB_FMT "\033[38;2;%d;%d;%dm"
#define SANSI_COLOR_BG_RGB_FMT "\033[48;2;%d;%d;%dm"
#define SANSI_MOVE_CURSOR_ABS_FMT "\033[%d;%dH"

#define array_alloc(elem_type, num_elem) (elem_type*)calloc(num_elem, sizeof(elem_type))

size_t _array_size(void** array) {
    size_t size = 0;
    if (array) {
        for (; array[size] != NULL; size++) { }
    }
    return size;
}
#define array_size(array) _array_size((void**)(array))


void** _array_append(void** array, void* element) {
    // Find the current size of the array
    size_t size = array_size(array);

    // Resize the array to make space for the new element plus the NULL terminator
    void** new_array = realloc(array, (size + 2) * sizeof(void*));
    if (new_array == NULL) {
        // Memory allocation failed
        return NULL;
    }

    // Append the new element and re-terminate the array
    new_array[size] = element;
    new_array[size + 1] = NULL;

    return new_array;
}
#define array_append(array, element) _array_append((void**)(array), element)


void _array_remove(void** array, void* element) {
    for (size_t i = 0; array[i] != NULL; i++) {
        if (array[i] == element) {
            // Found the element to remove, shift all subsequent element back one slot
            for (; array[i] != NULL; i++) {
                array[i] = array[i + 1];
            }
            break;
        }
    }
}
#define array_remove(array, element) _array_remove((void**)array, element)


typedef struct {
    nsansi_color_t fg;
    nsansi_color_t bg;
    wchar_t c;
    uint8_t attributes;
} cell_t;


static int point_inside_region(sansi_rowcol_t point, sansi_rowcol_t pos, sansi_rowcol_t size) {
    int in_col = pos.column <= point.column && point.column < pos.column + size.column;
    int in_row = pos.row <= point.row && point.row < pos.row + size.row;
    return in_col && in_row;
}


static nsansi_screen_t* screen_new() {
    nsansi_screen_t* s = (nsansi_screen_t*)malloc(sizeof(nsansi_screen_t));
    if (s == NULL)
        return NULL;

    s->size = sansi_terminal_size();
    
    size_t num_elems = (size_t)(s->size.row * s->size.column);

    // For each character on one row, we will need to at most output the character and the foreground and
    // background color (and the null terminator). Additionally, the length of the color changing code is
    // the same for foreground and background, and is at most 3 character longer than the formatting strings
    // defined (because there are 3 %d:s in the formatting string, and each of those can be replaced with a number
    // between 0-255). Same logic for the formatting string for moving the cursor.
    size_t max_buffer_size = s->size.column * (sizeof(wchar_t) + (strlen(SANSI_COLOR_BG_RGB_FMT) + 3) * 2) + (strlen(SANSI_MOVE_CURSOR_ABS_FMT) + 2) + 1;

    s->text = array_alloc(wchar_t, num_elems);
    s->attributes = array_alloc(uint8_t, num_elems);
    s->fgcolor = array_alloc(nsansi_color_t, num_elems);
    s->bgcolor = array_alloc(nsansi_color_t, num_elems);
    s->output_buffer = array_alloc(uint8_t, max_buffer_size); // Pre-allocate a buffer for outputting a single row with one call to fputs.
    
    // Initialize the text array to empty space.
    // NOTE: other arrays above are zero-initialized.
    for (size_t i = 0; i < num_elems; i++)
        s->text[i] = L' ';

    return s;
}

void screen_free(nsansi_screen_t* s) {
    if (s == NULL)
        return; // Nothing to free.

    free(s->text);
    free(s->attributes);
    free(s->fgcolor);
    free(s->bgcolor);
    free(s->output_buffer);
    free(s);
}



nsansi_window_t* nsansi_window_screen() {
    nsansi_window_t* w = (nsansi_window_t*)malloc(sizeof(nsansi_window_t));
    if (w == NULL)
        return NULL;

    w->screen = screen_new();
    w->pos = (sansi_rowcol_t){0, 0};
    w->size = w->screen->size;
    w->visible = 1;

    w->regions = array_alloc(nsansi_region_t*, 1); // Pre-allocate space for 1 region
    w->windows = array_alloc(nsansi_window_t*, 1); // Pre-allocate space for 1 nested window
    w->parent = NULL;
    
    return w;
}

nsansi_window_t* nsansi_window_new_subwindow(nsansi_window_t* parent, sansi_rowcol_t pos, sansi_rowcol_t size) {
    if (parent == NULL)
        return NULL;

    sansi_rowcol_t abspos = (sansi_rowcol_t){parent->pos.row + pos.row, parent->pos.column + pos.column};

    sansi_rowcol_t corners[] = {{abspos.row, abspos.column}, {abspos.row, abspos.column + size.column - 1},
                                {abspos.row + size.row - 1, abspos.column}, {abspos.row + size.row  - 1, abspos.column + size.column - 1}};

    for (int i = 0; i < 4; i++) {
        sansi_rowcol_t c = corners[i];
        if (!point_inside_region(c, parent->pos, parent->size)) {
            errno = EINVAL;
            return NULL;
        }
    }
    
    nsansi_window_t* w = (nsansi_window_t*)malloc(sizeof(nsansi_window_t));
    if (w == NULL)
        return NULL;

    w->screen = parent->screen;
    w->size = size;
    w->pos = abspos;
    w->visible = 1;
    
    w->regions = array_alloc(nsansi_region_t*, 1); // Pre-allocate space for 1 region
    w->windows = array_alloc(nsansi_window_t*, 1); // Pre-allocate space for 1 nested window
    w->parent = parent;
    array_append(parent->windows, w);
    return w;
}


void nsansi_window_free(nsansi_window_t* window) {
    if (window->parent == NULL)  // If this is the top-most window; also free the screen.
        screen_free(window->screen);

    // Free all nested regions
    if (window->regions != NULL) {
        for (size_t i = 0; window->regions[i]; i++) {
            free(window->regions[i]);
        }
        free(window->regions);
    }

    // Free all nested windows
    if (window->windows != NULL) {
        for (size_t i = 0; window->windows[i]; i++) {
            nsansi_window_free(window->windows[i]);
        }
        free(window->windows);
    }

    if (window->parent != NULL)
        array_remove(window->parent->windows, window);

    // Free the window itself
    free(window);
}

nsansi_region_t* nsansi_window_new_region(nsansi_window_t* window, sansi_rowcol_t pos, sansi_rowcol_t size) {

    if (pos.row < 0 || pos.column < 0 || pos.row + size.row > window->size.row || pos.column + size.column > window->size.column) {
        errno = EINVAL;
        return NULL;
    }

    sansi_rowcol_t abspos = (sansi_rowcol_t){window->pos.row + pos.row, window->pos.column + pos.column};
    sansi_rowcol_t corners[] = {{abspos.row, abspos.column}, {abspos.row, abspos.column + size.column - 1},
                                {abspos.row + size.row - 1, abspos.column}, {abspos.row + size.row  - 1, abspos.column + size.column - 1}};

    for (int idx = 0; window->regions[idx] != NULL; idx++) {
        nsansi_region_t* region = window->regions[idx];
        for (int i = 0; i < 4; i++) {
            sansi_rowcol_t c = corners[i];
            if (point_inside_region(c, region->pos, region->size)) {
                errno = EINVAL;
                return NULL;
            }
        }
    }

    // Allocate memory for the new region.
    nsansi_region_t* region = (nsansi_region_t*)malloc(sizeof(nsansi_region_t));
    if (region == NULL) {
        // Handle memory allocation failure
        return NULL;
    }

    region->window = window;
    region->pos = abspos;
    region->size = size;
    region->row_dirty = array_alloc(uint8_t, (size_t)size.row);
    memset(region->row_dirty, 1, size.row);
    region->dirty = 1;

    // TODO: Handle allocation failure
    window->regions = (nsansi_region_t**)array_append(window->regions, region);
    return region;
}

void nsansi_window_free_region(nsansi_window_t* window, nsansi_region_t* region) {
    array_remove(window->regions, region);
    free(region->row_dirty);
    free(region);
}

static uint8_t* write_base10_ascii(int number, uint8_t* buffer) {
    if (number >= 100) {
        *buffer++ = (uint8_t)('0' + (number / 100));
        *buffer++ = (uint8_t)('0' + ((number % 100) / 10));
        *buffer++ = (uint8_t)('0' + (number % 10));
    } else if (number >= 10) {
        *buffer++ = (uint8_t)('0' + (number / 10));
        *buffer++ = (uint8_t)('0' + (number % 10));
    } else {
        *buffer++ = (uint8_t)('0' + number);
    }
    return buffer;
}

static uint8_t* write_cursor_abs(int row, int col, uint8_t* buffer) {
    *buffer++ = '\033';
    *buffer++ = '[';
    buffer = write_base10_ascii(row + 1, buffer);
    *buffer++ = ';';
    buffer = write_base10_ascii(col + 1, buffer);
    *buffer++ = 'H';
    return buffer;
}

// fg_or_bg = '3' -> forground
// fg_or_bg = '4' -> background
static uint8_t* write_color_rgb(uint8_t* buffer, char fg_or_bg, uint8_t r, uint8_t g, uint8_t b) {
    *buffer++ = '\033';
    *buffer++ = '[';
    *buffer++ = fg_or_bg;
    *buffer++ = '8';
    *buffer++ = ';';
    *buffer++ = '2';
    *buffer++ = ';';
    buffer = write_base10_ascii(r, buffer);
    *buffer++ = ';';
    buffer = write_base10_ascii(g, buffer);
    *buffer++ = ';';
    buffer = write_base10_ascii(b, buffer);
    *buffer++ = 'm';
    return buffer;
}

uint8_t* write_wchar(uint8_t* buffer, wchar_t wchar) {
    int num_bytes = 0;
    uint32_t charCode = (uint32_t)wchar;

    if (charCode <= 0x7F) {
        // 1-byte sequence
        buffer[0] = (unsigned char)charCode;
        num_bytes = 1;
    } else if (charCode <= 0x7FF) {
        // 2-byte sequence
        buffer[0] = 0xC0 | (unsigned char)(charCode >> 6);
        buffer[1] = 0x80 | (unsigned char)(charCode & 0x3F);
        num_bytes = 2;
    } else if (charCode <= 0xFFFF) {
        // 3-byte sequence
        buffer[0] = 0xE0 | (unsigned char)(charCode >> 12);
        buffer[1] = 0x80 | (unsigned char)((charCode >> 6) & 0x3F);
        buffer[2] = 0x80 | (unsigned char)(charCode & 0x3F);
        num_bytes = 3;
    } else if (charCode <= 0x10FFFF) {
        // 4-byte sequence
        buffer[0] = 0xF0 | (unsigned char)(charCode >> 18);
        buffer[1] = 0x80 | (unsigned char)((charCode >> 12) & 0x3F);
        buffer[2] = 0x80 | (unsigned char)((charCode >> 6) & 0x3F);
        buffer[3] = 0x80 | (unsigned char)(charCode & 0x3F);
        num_bytes = 4;
    } else {
        // Character code out of range
        assert(0);
    }

    return buffer + num_bytes;
}

int color_eq(nsansi_color_t c0, nsansi_color_t c1) {
    return (c0.r == c1.r) && (c0.g == c1.g) && (c0.b == c1.b);
}



static void window_sync_only(nsansi_window_t* window) {
    nsansi_screen_t* screen = window->screen;

    // This buffer is pre-allocated to have enough space for one row.
    // We do not need to bound check any writes to it
    uint8_t* begin = screen->output_buffer;

    for (int idx = 0; window->regions[idx] != NULL; idx++) {
        nsansi_region_t* region = window->regions[idx];
        if (!region->dirty)
            continue;
        region->dirty = 0;

        for (int row = 0; row < region->size.row; ++row) {
            if (!region->row_dirty[row])
                continue;
            region->row_dirty[row] = 0;

            uint8_t* buffer = begin;

            int actual_row = row + region->pos.row;
            int actual_col = region->pos.column;
            buffer = write_cursor_abs(actual_row, actual_col, buffer);   

            // Always emit the color codes for the first character on the row.
            nsansi_color_t curr_fg = screen->fgcolor[actual_row * screen->size.column + actual_col];
            nsansi_color_t curr_bg = screen->bgcolor[actual_row * screen->size.column + actual_col];
            buffer = write_color_rgb(buffer, '3', curr_fg.r, curr_fg.g, curr_fg.b);
            buffer = write_color_rgb(buffer, '4', curr_bg.r, curr_bg.g, curr_bg.b);

            for (int col = 0; col < region->size.column; ++col) {
                int index = actual_row * screen->size.column + col + actual_col;
                nsansi_color_t fg = screen->fgcolor[index];
                nsansi_color_t bg = screen->bgcolor[index];

                if (!color_eq(curr_fg, fg)) {
                    buffer = write_color_rgb(buffer, '3', fg.r, fg.g, fg.b);
                    curr_fg = fg;
                }
                if (!color_eq(curr_bg, bg)) {
                    buffer = write_color_rgb(buffer, '4', bg.r, bg.g, bg.b);
                    curr_bg = bg;
                }
                buffer = write_wchar(buffer, screen->text[index]);
            }
            size_t num_bytes = buffer - begin;
            fwrite(begin, 1, num_bytes, stdout);
        }
    }

    for (int i = 0; window->windows[i] != NULL; i++)
        window_sync_only(window->windows[i]);

}


void nsansi_window_sync(nsansi_window_t* window) {
    if (!window)
        return;

    sansi_cursor_save_pos();
    window_sync_only(window);

    sansi_sync();  // Flush changes to the terminal.
}


void nsansi_region_print(nsansi_region_t* region, sansi_rowcol_t pos, const wchar_t* string, int width, wchar_t pad) {
    if (region == NULL)
        return;

    if ((pos.row >= region->size.row) || (pos.column >= region->size.column))
        return;

    nsansi_window_t* win = region->window;
    nsansi_screen_t* screen = win->screen;

    int window_width = screen->size.column;
    int max_char_write = window_width - pos.column;
    if ((width >= 0) && (width < max_char_write))
        max_char_write = width;

    int start_idx = (region->pos.row + pos.row) * window_width + region->pos.column + pos.column; // Calculate the start index in the text array.
    int i = 0;

    // Write characters until we hit the end of the string or reach the specified width limit or layer's bounds.
    for (; string[i] != L'\0' && (i < max_char_write); i++)
        screen->text[start_idx + i] = string[i];

    // If width is specified and there's remaining space, pad it.
    for (; i < max_char_write; i++)
        screen->text[start_idx + i] = pad;

    // Mark row as dirty.
    region->row_dirty[pos.row] = 1;

    // Mark the layer as dirty for redraw.
    region->dirty = 1;
}


void nsansi_region_scroll(nsansi_region_t* region, sansi_rowcol_t delta) {
    if (region == NULL || (delta.row == 0 && delta.column == 0))
        return;

    nsansi_window_t* win = region->window;
    nsansi_screen_t* screen = win->screen;
    int row, col, idx, new_idx, start, end, step;
    wchar_t default_char = L' ';
    nsansi_color_t default_fg = {255, 255, 255, 255}; // Default foreground: white
    nsansi_color_t default_bg = {0, 0, 0, 255};       // Default background: black

    // Vertical scrolling
    if (delta.row != 0) {
        start = (delta.row > 0) ? (region->size.row - 1) : 0;
        end = (delta.row > 0) ? -1 : region->size.row;
        step = (delta.row > 0) ? -1 : 1;

        for (row = start; row != end; row += step) {
            for (col = 0; col < region->size.column; col++) {
                idx = ((region->pos.row + row) * screen->size.column) + (region->pos.column + col);
                new_idx = ((region->pos.row + row - delta.row) * screen->size.column) + (region->pos.column + col);

                if (row - delta.row >= 0 && row - delta.row < region->size.row) {
                    screen->text[idx] = screen->text[new_idx];
                    screen->fgcolor[idx] = screen->fgcolor[new_idx];
                    screen->bgcolor[idx] = screen->bgcolor[new_idx];
                } else {
                    screen->text[idx] = default_char;
                    screen->fgcolor[idx] = default_fg;
                    screen->bgcolor[idx] = default_bg;
                }
            }
        }
    }

    // Horizontal scrolling
    if (delta.column != 0) {
        start = (delta.column > 0) ? (region->size.column - 1) : 0;
        end = (delta.column > 0) ? -1 : region->size.column;
        step = (delta.column > 0) ? -1 : 1;

        for (col = start; col != end; col += step) {
            for (row = 0; row < region->size.row; row++) {
                idx = ((region->pos.row + row) * screen->size.column) + (region->pos.column + col);
                new_idx = ((region->pos.row + row) * screen->size.column) + (region->pos.column + col - delta.column);

                if (col - delta.column >= 0 && col - delta.column < region->size.column) {
                    screen->text[idx] = screen->text[new_idx];
                    screen->fgcolor[idx] = screen->fgcolor[new_idx];
                    screen->bgcolor[idx] = screen->bgcolor[new_idx];
                } else {
                    screen->text[idx] = default_char;
                    screen->fgcolor[idx] = default_fg;
                    screen->bgcolor[idx] = default_bg;
                }
            }
        }
    }

    memset(region->row_dirty, 1, region->size.row);
    region->dirty = 1; // Mark the region as dirty to redraw it.
}



void nsansi_region_paint(nsansi_region_t* region, sansi_rowcol_t pos, sansi_rowcol_t size, nsansi_color_t color, int foreground) {
    if (region == NULL)
        return;

    // Get the parent regions dimensions to avoid writing outside bounds.
    nsansi_window_t* win = region->window;
    nsansi_screen_t* screen = win->screen;

    int start_row = region->pos.row + pos.row;
    int end_row = start_row + size.row;
    if (start_row > win->size.row)
        return;
    if (end_row > win->size.row)
        end_row = win->size.row;

    int start_column = region->pos.column + pos.column;
    int end_column = start_column + size.column;
    if (start_column > win->size.column)
        return;
    if (end_column > win->size.column)
        end_column = win->size.column;

    nsansi_color_t* colors = foreground ? screen->fgcolor : screen->bgcolor;
    // Iterate over the rectangle defined by pos (starting position) and size (dimensions).
    for (int row = start_row; row < end_row; ++row) {
        for (int column = start_column; column < end_column; ++column) {
            int index = row * screen->size.column + column;
            colors[index] = color;
        }
        region->row_dirty[row - region->pos.row] = 1;
    }


    // Mark the region as dirty so it will be redrawn.
    region->dirty = 1;
}


void nsansi_region_paint_all(nsansi_region_t* region, nsansi_color_t color, int foreground) {
    if (region == NULL)
        return;

    nsansi_region_paint(region, (sansi_rowcol_t){0, 0}, region->size, color, foreground);
}
