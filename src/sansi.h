#ifndef _SANSI_H_
#define _SANSI_H_
#include <stdint.h>

typedef struct {
    int row;
    int column;
} sansi_rowcol_t;

typedef struct {
    int r;
    int g;
    int b;
} sansi_color_t;


// Gets the terminal size.
sansi_rowcol_t sansi_terminal_size();

// Reset the terminal state.
void sansi_reset();

// This function simply delegates to printf,, and is not particularly useful in C,
// however, when binding to other languages calling this function instead of 
// 'print' in the language ensures that all output end up in the same order.
// Otherwise, buffering might cause things to end up out of order.
void sansi_writew(int width, const char* string);

// Flush stdout
void sansi_sync();

// Enter/exit raw mode
void sansi_mode_raw(int enable);

// Check if raw mode is enabled
int sansi_mode_raw_enabled();

// Enter/exit application mode (alternate screen buffer)
void sansi_mode_application(int enable);

// Switch to the alternative screen (enable = 1) or the
// main screen (enable = 0)
void sansi_mode_alternative_screen(int enable);



// Get the current location of the cursor, where (0, 0) is home.
// NOTE: Reading the cursor position is a combinatioin of writing to
// stdout followed by reading from stdin. This means that if there is
// any data waiting to be read on stdin, thus function reads and ignores
// that data before it can query for the cursor location.
// What this all means is that you have to options:
//   1) call sansi_read_key() until it returns nothing before calling this function; or
//   2) accept that you might miss a key press.
sansi_rowcol_t sansi_cursor_query();

// Makes the cursor invisible.
void sansi_cursor_hide();

// Makes the cursor visible.
void sansi_cursor_show();

// Moves the cursor up by 'lines' lines.
// Example: sansi_cursor_up(1); // Moves the cursor up by 1 line
void sansi_cursor_up(int lines);

// Moves the cursor down by 'lines' lines.
// Example: sansi_cursor_down(1); // Moves the cursor down by 1 line
void sansi_cursor_down(int lines);

// Moves the cursor to the right by 'columns' columns.
void sansi_cursor_right(int columns);

// Moves the cursor to the left by 'columns' columns.
void sansi_cursor_left(int columns);

// Sets the cursor to a specific position (row, column).
// NOTE: (0, 0) is home.
void sansi_cursor_abs(int row, int column);

// Saves the current cursor position.
void sansi_cursor_save_pos();

// Restores the cursor position to the last saved position.
void sansi_cursor_restore_pos();

// Moves cursor to home position (0, 0).
void sansi_cursor_home();

// Moves cursor to the beginning of the next line, 'lines' lines down.
void sansi_cursor_bol_below(int lines);

// Moves cursor to the beginning of the previous line, 'lines' lines up.
void sansi_cursor_bol_above(int lines);

// Moves cursor to column #.
void sansi_cursor_move_column(int column);

// Erase from cursor to end of screen.
void sansi_erase_below();

// Erase from cursor to beginning of screen.
void sansi_erase_above();

// Erase entire screen.
void sansi_erase_screen();

// Erase saved lines (scrollback buffer, may not work on all terminals).
void sansi_erase_saved_lines();

// Erase from cursor to end of line.
void sansi_erase_to_eol();

// Erase from start of line to the cursor.
void sansi_erase_to_bol();

// Erase the entire line.
void sansi_erase_line();


// Scroll the terminal up
void sansi_scroll_up(int lines);

// Scroll the terminal down
void sansi_scroll_down(int lines);


// Set foreground color using RGB (0-255) values.
void sansi_color_fg_rgb(sansi_color_t color);

// Set background color using RGB values.
void sansi_color_bg_rgb(sansi_color_t color);



// Reset all styles and colors to default.
void sansi_style_reset();

// Toggle bold mode on or off.
void sansi_style_bold(int enable);

// Toggle dim/faint mode on or off.
void sansi_style_dim(int enable);

// Toggle italic mode on or off.
void sansi_style_italic(int enable);

// Toggle underline mode on or off.
void sansi_style_underline(int enable);

// Toggle blinking mode on or off.
void sansi_style_blinking(int enable);

// Toggle inverse/reverse mode on or off.
void sansi_style_inverse(int enable);

// Toggle hidden/invisible mode on or off.
void sansi_style_hidden(int enable);

// Toggle strikethrough mode on or off.
void sansi_style_strikethrough(int enable);



// Translate an 8 bit color image to sixel graphics. Output by writing to stdout.
char* sansi_pixels_display(const unsigned char* data, int width, int height,
                           const sansi_color_t* palette, int palette_size);

// If this terminal supports pixel graphics, get the size of the terminal in pixels.
sansi_rowcol_t sansi_pixels_terminal_size();

// Set the terminal window title.
// Example: sansi_window_title("My Application");
void sansi_window_title(const char* title);


// Returns the keycode for the pressed key (blocks until one is pressed).
// Assumes raw mode is enabled. 
//
// NOTE: this function effectively reads all availble data in STDIN, in
// otherwords, it assumes that there is at most one keypress waiting in STDIN.
// The implication here is that sansi_read_key() must be called frequently 
// enough to ensure that there are never more than one keypress waiting.
uint64_t sansi_read_key(double timout_sec);

#endif // _SANSI_H_
