#include <assert.h>
#include <errno.h>
#include <fcntl.h>
#include <stdarg.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/ioctl.h>
#include <sys/select.h>
#include <sys/time.h>
#include <termios.h>
#include <unistd.h>

#include "sansi.h"

// Function to get the terminal size
sansi_rowcol_t sansi_terminal_size() {
    struct winsize ws;
    sansi_rowcol_t dr;
    // Attempt to get the terminal size
    if (ioctl(STDOUT_FILENO, TIOCGWINSZ, &ws) == -1) {
        dr.column = -1;
        dr.row = -1;
    } else {
        // If ioctl succeeds, populate the terminal size struct
        dr.column = ws.ws_col;
        dr.row = ws.ws_row;
    }
    return dr;
}

void sansi_reset() {
    fputs("\033c", stdout);
}

void sansi_writew(int width, const char* string) {
    if (width < 0)
        fputs(string, stdout);
    else
        printf("%-*.*s", width, width, string);
}

void sansi_sync() {
    fflush(stdout);
}

// ----------------
//  MODE FUNCTIONS
// ----------------

// Enter raw mode
void sansi_mode_raw(int enable) {
    struct termios raw;
    
    // Get current terminal attributes
    tcgetattr(STDIN_FILENO, &raw);
    
    tcflag_t options = (ECHO | ICANON | ISIG);
    if (enable) {
        // Disable ECHO, canonical mode, and signals
        raw.c_lflag &= ~options;
        
        // Disable the start (XON) and stop (XOFF) control characters
        // CTRL-Q is typically the start control character (XON)
        raw.c_cc[VSTART] = 0;
        raw.c_cc[VSTOP] = 0;
    } else {
        // Enable ECHO, canonical mode, and signals
        raw.c_lflag |= options;
        // Disable the start (XON) and stop (XOFF) control characters
        // CTRL-Q is typically the start control character (XON)
        raw.c_cc[VSTART] = 1;
        raw.c_cc[VSTOP] = 1;
    }

    // Apply the modified settings to enter raw mode
    tcsetattr(STDIN_FILENO, TCSAFLUSH, &raw);
}

// Check if terminal is in raw mode.
int sansi_mode_raw_enabled() {
    struct termios raw;
    
    // Get current terminal attributes
    tcgetattr(STDIN_FILENO, &raw);
    
    tcflag_t options = (ECHO | ICANON | ISIG);
    return (raw.c_lflag & options) == 0;
}


// Enter application mode (alternate screen buffer)
void sansi_mode_application(int enable) {
    if (enable) {
        // Switch to the alternate screen buffer
        fputs("\033[?1049h", stdout);
        // Application keypad mode (if needed)
        fputs("\033[?1h\033=", stdout);
    } else {
        // Return to the normal screen buffer
        fputs("\033[?1049l", stdout);
        // Normal keypad mode (if needed)
        fputs("\033[?1l\033>", stdout);
    }
}

// Switch to the alternative screen
void sansi_mode_alternative_screen(int enable) {
    if (enable)
        fputs("\033[?1049h", stdout);
    else
        fputs("\033[?1049l", stdout);
}

// ------------------
//  CURSOR FUNCTIONS
// ------------------


static inline void flush_stdin() {
    int flags = fcntl(STDIN_FILENO, F_GETFL, 0);
    fcntl(STDIN_FILENO, F_SETFL, flags | O_NONBLOCK);

    char buf;
    while (read(STDIN_FILENO, &buf, 1) > 0) { }

    // Restore flags
    fcntl(STDIN_FILENO, F_SETFL, flags);
}


sansi_rowcol_t sansi_cursor_query() {
    char buf[32];
    unsigned int i = 0;

    struct termios orig_termios, tmp_termios;
    tcgetattr(STDIN_FILENO, &orig_termios);
    tmp_termios = orig_termios;
    tmp_termios.c_lflag &= (tcflag_t)(~(ICANON | ECHO));
    tcsetattr(STDIN_FILENO, TCSANOW, &tmp_termios);

    // Flush stdin to ensure it's empty
    flush_stdin();

    // Send the query cursor location ANSI escape code
    printf("\033[6n");
    fflush(stdout);

    // Read the response: ESC[row;colR
    while (i < sizeof(buf) - 1) {
        if (read(STDIN_FILENO, buf + i, 1) != 1) break;
        if (buf[i] == 'R') break;
        i++;
    }
    buf[i] = '\0';
    tcsetattr(STDIN_FILENO, TCSANOW, &orig_termios);

    int row = -1, col = -1;
    // Parse the response to get row and column
    if (buf[0] == '\033' && buf[1] == '[')
        sscanf(buf + 2, "%d;%d", &row, &col);
    // Terminals use (1, 1) as home; sansi use (0, 0) as home.
    return (sansi_rowcol_t){.column=col - 1, .row=row - 1};
}

void sansi_cursor_hide() {
    fputs("\033[?25l", stdout);
}

void sansi_cursor_show() {
    fputs("\033[?25h", stdout);
}

// Moves the cursor up by 'lines' lines.
void sansi_cursor_up(int lines) {
    printf("\033[%dA", lines);
}

// Moves the cursor down by 'lines' lines.
void sansi_cursor_down(int lines) {
    printf("\033[%dB", lines);
}

// Moves the cursor to the right by 'columns' columns
void sansi_cursor_right(int columns) {
    printf("\033[%dC", columns);
}

// Moves the cursor to the left by 'columns' columns
void sansi_cursor_left(int columns) {
    printf("\033[%dD", columns);
}

// Sets the cursor to a specific position (row, column)
void sansi_cursor_abs(int row, int column) {
    // Terminals use (1, 1) as home; sansi use (0, 0) as home.
    printf("\033[%d;%dH", row + 1, column + 1);
}

// Saves the current cursor position
void sansi_cursor_save_pos() {
    fputs("\033[s", stdout);
}

// Restores the cursor position to the last saved position
void sansi_cursor_restore_pos() {
    fputs("\033[u", stdout);
}

// Moves cursor to home position (0, 0)
void sansi_cursor_home() {
    fputs("\033[H", stdout);
}

// Moves cursor to beginning of next line, # lines down
void sansi_cursor_bol_below(int lines) {
    printf("\033[%dE", lines);
}

// Moves cursor to beginning of previous line, # lines up
void sansi_cursor_bol_above(int lines) {
    printf("\033[%dF", lines);
}

// Moves cursor to column #
void sansi_cursor_move_column(int column) {
    printf("\033[%dG", column);
}



// -------------------
//  ERASING FUNCTIONS
// -------------------

// Erase from cursor to end of screen
void sansi_erase_below() {
    fputs("\033[0J", stdout);
}

// Erase from cursor to beginning of screen
void sansi_erase_above() {
    fputs("\033[1J", stdout);
}

// Erase entire screen
void sansi_erase_screen() {
    fputs("\033[2J", stdout);
}

// Erase saved lines (scrollback buffer, may not work on all terminals)
void sansi_erase_saved_lines() {
    fputs("\033[3J", stdout);
}

// Erase from cursor to end of line
void sansi_erase_to_eol() {
    fputs("\033[0K", stdout);
}

// Erase from start of line to the cursor
void sansi_erase_to_bol() {
    fputs("\033[1K", stdout);
}

// Erase the entire line
void sansi_erase_line() {
    fputs("\033[2K", stdout);
}



// ------------------
//  SCROLL FUNCTIONS
// ------------------

void sansi_scroll_up(int lines) {
    printf("\033[%dS", lines);
}

void sansi_scroll_down(int lines) {
    printf("\033[%dT", lines);
}



// -----------------
//  COLOR FUNCTIONS
// -----------------

void sansi_color_fg_rgb(sansi_color_t color) {
    printf("\033[38;2;%d;%d;%dm", color.r, color.g, color.b);
}

void sansi_color_bg_rgb(sansi_color_t color) {
    printf("\033[48;2;%d;%d;%dm", color.r, color.g, color.b);
}


// -----------------
//  STYLE FUNCTIONS
// -----------------

// Reset all modes (styles and colors)
void sansi_style_reset() {
    fputs("\033[0m", stdout);
}

// General function to apply or remove a style
static inline void sansi_style_apply(int enable, const char* enableCode, const char* disableCode) {
    printf("\033[%sm", enable ? enableCode : disableCode);
}

// Set or unset bold mode
void sansi_style_bold(int enable) {
    sansi_style_apply(enable, "1", "22");
}

// Set or unset dim/faint mode
void sansi_style_dim(int enable) {
    sansi_style_apply(enable, "2", "22");
}

// Set or unset italic mode
void sansi_style_italic(int enable) {
    sansi_style_apply(enable, "3", "23");
}

// Set or unset underline mode
void sansi_style_underline(int enable) {
    sansi_style_apply(enable, "4", "24");
}

// Set or unset blinking mode
void sansi_style_blinking(int enable) {
    sansi_style_apply(enable, "5", "25");
}

// Set or unset inverse/reverse mode
void sansi_style_inverse(int enable) {
    sansi_style_apply(enable, "7", "27");
}

// Set or unset hidden/invisible mode
void sansi_style_hidden(int enable) {
    sansi_style_apply(enable, "8", "28");
}

// Set or unset strikethrough mode
void sansi_style_strikethrough(int enable) {
    sansi_style_apply(enable, "9", "29");
}


// ----------------------------
//  SIXEL (GRAPHICS) FUNCTIONS
// ----------------------------
// Dynamic string buffer
typedef struct {
    char* buffer;
    size_t size;
    size_t capacity;
} dynamic_string;

// Function to initialize the dynamic string
void dynamic_string_init(dynamic_string* ds) {
    ds->capacity = 1024; // Initial capacity
    ds->size = 0;
    ds->buffer = (char*)malloc(ds->capacity * sizeof(char));
    if (ds->buffer == NULL) {
        perror("Failed to allocate buffer");
        exit(1);
    }
    ds->buffer[0] = '\0'; // Null-terminate the empty string
}

// Function to append formatted string to the dynamic string
void dynamic_string_append(dynamic_string* ds, const char* format, ...) {
    va_list args;
    va_start(args, format);

    // Determine the required size
    size_t required = (size_t)vsnprintf(NULL, 0, format, args);
    va_end(args);

    if (ds->size + required >= ds->capacity) {
        size_t new_capacity = ds->capacity * 2 + required;
        char* new_buffer = (char*)realloc(ds->buffer, new_capacity);
        if (new_buffer == NULL) {
            perror("Failed to reallocate buffer");
            exit(1);
        }
        ds->buffer = new_buffer;
        ds->capacity = new_capacity;
    }

    // Append the new content
    va_start(args, format);
    vsprintf(ds->buffer + ds->size, format, args);
    va_end(args);

    ds->size += required;
}

// Functions adapted to use dynamic_string_append instead of direct output
void enter_sixel_mode(dynamic_string* ds) {
    dynamic_string_append(ds, "\x1BP7;1;q");
}

void exit_sixel_mode(dynamic_string* ds) {
    dynamic_string_append(ds, "\x1B\\");
}

void set_colormap_register(dynamic_string* ds, int regidx, int r, int g, int b) {
    dynamic_string_append(ds, "#%d;2;%d;%d;%d", regidx, r, g, b);
}

char encode_band(unsigned char pixels[6], int color) {
    int value = 63;
    for (int i = 0; i < 6; i++) {
        value += (pixels[i] == color) << i;
    }
    return (char)value;
}

void select_encoding(dynamic_string* ds, int repeat, char ch) {
    if (repeat < 3) {
        while (repeat--) dynamic_string_append(ds, "%c", ch);
    } else {
        while (repeat > 255) {
            dynamic_string_append(ds, "!255%c", ch);
            repeat -= 255;
        }
        if (repeat > 0)
            dynamic_string_append(ds, "!%d%c", repeat, ch);
    }
}

// Main function to generate sixel graphics, now returning a dynamically allocated string
char* sansi_pixels_display(const unsigned char* data, int width, int height, const sansi_color_t* palette, int palette_size) {
    dynamic_string ds;
    dynamic_string_init(&ds);

    enter_sixel_mode(&ds);

    for (int i = 1; i < palette_size; i++) {
        set_colormap_register(&ds, i, palette[i].r * 100 / 255, palette[i].g * 100 / 255, palette[i].b * 100 / 255);
    }

    for (int y = 0; y < height; y += 6) {
        for (int color = 0; color < palette_size; color++) {
            int prev = -1;
            int repeat = 0;
            dynamic_string_append(&ds, "#%d", color);
            for (int x = 0; x < width; x++) {
                char ch = 63;
                if (color != 0) {
                    unsigned char band[6] = {0};
                    for (int dy = 0; dy < 6 && (y + dy) < height; dy++)
                        band[dy] = data[(y + dy) * width + x];
                    ch = encode_band(band, color);
                }
                if (ch == prev) {
                    repeat++;
                } else {
                    if (prev != -1)
                        select_encoding(&ds, repeat, (char)prev);
                    prev = ch;
                    repeat = 1;
                }
            }
            if (prev != -1)
                select_encoding(&ds, repeat, (char)prev);
            dynamic_string_append(&ds, "$");
        }
        dynamic_string_append(&ds, "-");
    }

    exit_sixel_mode(&ds);

    // Ensure the buffer is null-terminated
    if (ds.size == ds.capacity) {
        // Buffer is exactly full, need one more byte for null terminator
        char* new_buffer = realloc(ds.buffer, ds.size + 1);
        if (new_buffer == NULL) {
            perror("Failed to reallocate buffer for null terminator");
            exit(1);
        }
        ds.buffer = new_buffer;
    }
    ds.buffer[ds.size] = '\0';

    return ds.buffer;
}

// Usage example:
// char* sixel_data = sansi_pixels_display(data, width, height, palette, palette_size);
// // Use sixel_data...
// free(sixel_data);



// Function to get the terminal size in pixels
sansi_rowcol_t sansi_pixels_terminal_size() {
    struct winsize ws;
    sansi_rowcol_t dr;
    // Attempt to get the terminal size
    if (ioctl(STDOUT_FILENO, TIOCGWINSZ, &ws) == -1) {
        dr.column = -1;
        dr.row = -1;
    } else {
        // If ioctl succeeds, populate the terminal size struct
        dr.column = ws.ws_xpixel;
        dr.row = ws.ws_ypixel;
    }
    return dr;
}



// ----------------
//  MISC FUNCTIONS
// ----------------

// Function to set the terminal window title
void sansi_window_title(const char* title) {
    printf("\033]0;%s\a", title);
}


// -----------------
//  INPUT FUNCTIONS
// -----------------


uint64_t sansi_read_key(double timeout_sec) {
    assert(sansi_mode_raw_enabled());
    uint64_t key = 0;
    fd_set readfds;
    int retval;
    ssize_t bytes_read;
    int nothing_read = 1;

    // Calculate total microseconds for the user-defined timeout
    // If timeout is 0, we'll use this value to wait indefinitely
    long total_microseconds = timeout_sec > 0 ? (long)(timeout_sec * 10000000) : 0;
    long elapsed_microseconds = 0;

    do {
        FD_ZERO(&readfds);
        FD_SET(STDIN_FILENO, &readfds);

        // This is the constant timeout used for select()
        struct timeval tv;
        tv.tv_sec = 0;
        tv.tv_usec = 100; // Check for input every 0.1 millisecond

        retval = select(STDIN_FILENO + 1, &readfds, NULL, NULL, &tv);

        if (retval > 0) {
            char c;
            bytes_read = read(STDIN_FILENO, &c, 1);
            if (bytes_read > 0) {
                key = (key << 8) | (unsigned char)c;
                nothing_read = 0;
            }
        }

        // Update elapsed time if a timeout is specified
        if (timeout_sec > 0) {
            elapsed_microseconds += 1000; // Increment by the select() timeout duration
            if (elapsed_microseconds >= total_microseconds) {
                break; // Exit if the specified timeout has elapsed
            }
        }
    // Continue if there's input or if waiting indefinitely (timeout == 0)
    } while ((retval > 0 && bytes_read > 0) || nothing_read || timeout_sec == 0);

    return key;
}
