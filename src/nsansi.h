#ifndef _NSANSI_H_
#define _NSANSI_H_
#include <stdint.h>
#include "sansi.h"
#include <wchar.h>

struct nsansi_window_s;

typedef struct {
    uint8_t r, g, b, a;
} nsansi_color_t;



typedef struct {
    struct nsansi_window_s* window;
    sansi_rowcol_t pos;
    sansi_rowcol_t size;
    
    // Whether each individual line needs to be redrawn.
    uint8_t* row_dirty;

    // If anything at all needs to be redrawn.
    int dirty;
} nsansi_region_t;


// Represents the state of the screen.
typedef struct {
    sansi_rowcol_t size;

    // The textual content of the window.
    wchar_t* text;

    // Bold, underline, blink, etc., for each cell of the window.
    uint8_t* attributes;

    // The forground color for each cell of the window
    nsansi_color_t* fgcolor;

    // The background color for each cell of the window.
    nsansi_color_t* bgcolor;

    // Pre-allocated buffer for buffering an entire row of output when syncing to the screen.
    uint8_t* output_buffer;

} nsansi_screen_t;



// Represents a drawable area on the terminal screen.
typedef struct nsansi_window_s {

    // Absolute position relative to (0, 0) on the screen.
    sansi_rowcol_t pos;

    // The size of the window.
    sansi_rowcol_t size;

    // Wheter the content of the window will be drawn to the screen
    // when calling nsansi_window_sync() on this window or any of
    // its parents.
    int visible;

    // The screen state.
    nsansi_screen_t* screen;

    // All non-overlapping regions that define this window.
    nsansi_region_t** regions;

    // All nested windows.
    struct nsansi_window_s** windows;

    // Parent window, or NULL if this is the top-most window.
    struct nsansi_window_s* parent;

} nsansi_window_t;




// Get the window that corresponds to the entire screen.
nsansi_window_t* nsansi_window_screen();


// Create a new window. A window must be entierly contained inside a single window, and can overlap with any other
// windows and regions.
nsansi_window_t* nsansi_window_new_subwindow(nsansi_window_t* parent, sansi_rowcol_t pos, sansi_rowcol_t size);

// Add a new region to this window. A region must be entierly contained inside a single window, and must
// not overlap with any other region in this window.
nsansi_region_t* nsansi_window_new_region(nsansi_window_t* window, sansi_rowcol_t pos, sansi_rowcol_t size);

// Free a region and remove it from its window.
void nsansi_window_free_region(nsansi_window_t* window, nsansi_region_t* region);

// Free all resources for a window, including all layers and nested windows.
// Will be removed from parent's array of nested windows.
void nsansi_window_free(nsansi_window_t* window);

// Syncs changes to the screen made since last call to sync()
void nsansi_window_sync(nsansi_window_t* window);

// Write some text to the text layer, at most 'width' characters.
// If string is shorter that 'width' the string is padded using 'pad'.
// If width=-1, no padding is done and no limit to the number of characters
// is enforced (other than the width of the window).
void nsansi_region_print(nsansi_region_t* region, sansi_rowcol_t pos, const wchar_t* string, int width, wchar_t pad);

// Scroll all layers of a region up/down/left/right.
// NOTE: Only one step in one direction is supported. That is, delta must be
//       one of: {1, 0}, {-1, 0}, {0, 1}, {0, -1}
void nsansi_region_scroll(nsansi_region_t* region, sansi_rowcol_t delta);

// Fill an area of cells of a color layer.
void nsansi_region_paint(nsansi_region_t* region, sansi_rowcol_t pos, sansi_rowcol_t size, nsansi_color_t color, int forground);

// Clear an entire color layer.
void nsansi_region_paint_all(nsansi_region_t* region, nsansi_color_t color, int foreground);

#endif // _NSANSI_H_