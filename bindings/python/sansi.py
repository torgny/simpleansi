import ctypes
import os

sansi_lib = ctypes.CDLL('/home/torgny/codespace/simpleansi/lib/libsansi.so')
libc = ctypes.CDLL(None)

# Define structures as per the C header file
class SansiRowCol(ctypes.Structure):
    _fields_ = [("row", ctypes.c_int),
                ("column", ctypes.c_int)]

class SansiColor(ctypes.Structure):
    _fields_ = [("r", ctypes.c_int),
                ("g", ctypes.c_int),
                ("b", ctypes.c_int)]

# Update function prototypes to match the C header file

# Terminal Size
sansi_lib.sansi_terminal_size.restype = SansiRowCol

def terminal_size():
    rc = sansi_lib.sansi_terminal_size()
    return (rc.row, rc.column) if rc.row >= 0 and rc.column >= 0 else (None, None)

def write(value, row=None, column=None, color=None, width=None):
    if color is not None:
        color_fg(color[0])
        color_bg(color[1])
    if row is not None:
        cursor_abs(row, column)
    
    if type(value) is str:
        value = value.encode('utf-8')
    sansi_lib.sansi_writew(ctypes.c_int(width or -1), value)

def sync():
    sansi_lib.sansi_sync()

# Mode Settings
def mode_raw(enable):
    sansi_lib.sansi_mode_raw(ctypes.c_int(enable))

sansi_lib.sansi_mode_raw_enabled.restype = ctypes.c_bool

def mode_raw_enabled():
    return sansi_lib.sansi_mode_raw_enabled()

def mode_application(enable):
    sansi_lib.sansi_mode_application(ctypes.c_int(enable))

def mode_alternative_screen(enable):
    sansi_lib.sansi_mode_alternative_screen(ctypes.c_int(enable))


# Cursor Visibility and Movement
def cursor_hide():
    sansi_lib.sansi_cursor_hide()

def cursor_show():
    sansi_lib.sansi_cursor_show()

def cursor_up(lines):
    sansi_lib.sansi_cursor_up(ctypes.c_int(lines))

def cursor_down(lines):
    sansi_lib.sansi_cursor_down(ctypes.c_int(lines))

def cursor_right(columns):
    sansi_lib.sansi_cursor_right(ctypes.c_int(columns))

def cursor_left(columns):
    sansi_lib.sansi_cursor_left(ctypes.c_int(columns))

def cursor_abs(row, column):
    sansi_lib.sansi_cursor_abs(ctypes.c_int(row), ctypes.c_int(column))

def cursor_save_pos():
    sansi_lib.sansi_cursor_save_pos()

def cursor_restore_pos():
    sansi_lib.sansi_cursor_restore_pos()

def cursor_home():
    sansi_lib.sansi_cursor_home()

def cursor_bol_below(lines):
    sansi_lib.sansi_cursor_bol_below(ctypes.c_int(lines))

def cursor_bol_above(lines):
    sansi_lib.sansi_cursor_bol_above(ctypes.c_int(lines))

def cursor_move_column(column):
    sansi_lib.sansi_cursor_move_column(ctypes.c_int(column))

# Screen and Line Erasing
def erase_below():
    sansi_lib.sansi_erase_below()

def erase_above():
    sansi_lib.sansi_erase_above()

def erase_screen():
    sansi_lib.sansi_erase_screen()

def erase_saved_lines():
    sansi_lib.sansi_erase_saved_lines()

def erase_to_eol():
    sansi_lib.sansi_erase_to_eol()

def erase_to_bol():
    sansi_lib.sansi_erase_to_bol()

def erase_line():
    sansi_lib.sansi_erase_line()

def erase_line():
    sansi_lib.sansi_erase_line()


def scroll_up(lines):
    sansi_lib.sansi_scroll_up(ctypes.c_int(lines))

def scroll_down(lines):
    sansi_lib.sansi_scroll_down(ctypes.c_int(lines))

# Color Functions
def color_fg(color):
    col = SansiColor(*color)
    sansi_lib.sansi_color_fg_rgb(col)

def color_bg(color):
    col = SansiColor(*color)
    sansi_lib.sansi_color_bg_rgb(col)

# Style Functions
def style_reset():
    sansi_lib.sansi_style_reset()

def style_bold(enable):
    sansi_lib.sansi_style_bold(ctypes.c_int(enable))

def style_dim(enable):
    sansi_lib.sansi_style_dim(ctypes.c_int(enable))

def style_italic(enable):
    sansi_lib.sansi_style_italic(ctypes.c_int(enable))

def style_underline(enable):
    sansi_lib.sansi_style_underline(ctypes.c_int(enable))

def style_blinking(enable):
    sansi_lib.sansi_style_blinking(ctypes.c_int(enable))

def style_inverse(enable):
    sansi_lib.sansi_style_inverse(ctypes.c_int(enable))

def style_hidden(enable):
    sansi_lib.sansi_style_hidden(ctypes.c_int(enable))

def style_strikethrough(enable):
    sansi_lib.sansi_style_strikethrough(ctypes.c_int(enable))


# Window Title
def window_title(title):
    sansi_lib.sansi_window_title(ctypes.c_char_p(title.encode('utf-8')))

# Key Read
sansi_lib.sansi_read_key.restype = ctypes.c_uint64
def read_key(timeout):
    c = sansi_lib.sansi_read_key(ctypes.c_double(timeout))
    if c == 0:
        return None
    return c


# Define the function prototype for sansi_pixels_display
sansi_lib.sansi_pixels_display.argtypes = [ctypes.POINTER(ctypes.c_ubyte), ctypes.c_int, ctypes.c_int, ctypes.POINTER(SansiColor), ctypes.c_int]
sansi_lib.sansi_pixels_display.restype = ctypes.c_char_p

def pixels_display(data, size, palette):
    width, height = size
    data_array = (ctypes.c_ubyte * len(data))(*data)
    palette_array = (SansiColor * len(palette))(*palette)
    
    # Call the C function
    result_ptr = sansi_lib.sansi_pixels_display(data_array, width, height, palette_array, len(palette))

    # Convert the returned C string to a Python 'bytes' object
    result_str = ctypes.string_at(result_ptr)
    
    # Free the allocated string using the C standard library's free function
    #libc.free(result_ptr)
    
    return result_str

# Terminal Size in pixels
sansi_lib.sansi_pixels_terminal_size.restype = SansiRowCol

def pixels_terminal_size():
    rc = sansi_lib.sansi_pixels_terminal_size()
    return (rc.row, rc.column) if rc.row >= 0 and rc.column >= 0 else None

